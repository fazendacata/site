import { Component, OnDestroy, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-your-project',
  templateUrl: './your-project.component.html',
  styleUrls: ['./your-project.component.scss'],
  providers: [MessageService],
})
export class YourProjectComponent implements OnInit, OnDestroy {
  items: MenuItem[] = [];

  subscription: Subscription = new Subscription();

  constructor(public messageService: MessageService) {}

  ngOnInit() {
    this.items = [
      {
        label: 'Informações Básicas',
        routerLink: 'steps/basic',
      },
      {
        label: 'Requisitos do Sistema',
        routerLink: 'steps/requirements',
      },
      {
        label: 'Informações Pessoais',
        routerLink: 'steps/personal',
      },
      {
        label: 'Confirmação',
        routerLink: 'steps/confirmation',
      },
    ];
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
