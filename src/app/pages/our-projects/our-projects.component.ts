import { Component } from '@angular/core';

@Component({
  selector: 'app-our-projects',
  templateUrl: './our-projects.component.html',
  styleUrls: ['./our-projects.component.scss'],
})
export class OurProjectsComponent {
  images: Array<{
    previewImageSrc: string;
    thumbnailImageSrc: string;
    title: string;
    alt: string;
    transparency?: boolean;
  }>;

  constructor() {
    this.images = [];
    this.images.push(
      {
        previewImageSrc: 'assets/images/cafepbc.png',
        thumbnailImageSrc: 'assets/images/logo_cafepbc.png',
        title: 'Café PBC',
        alt: 'Sistema de Produção, Beneficiamento e Comercialização de Café',
        transparency: true,
      },
      {
        previewImageSrc: 'assets/images/suldailha.png',
        thumbnailImageSrc: 'assets/images/logo_suldailha.png',
        title: 'Sul da Ilha Entregas',
        alt: 'Sistema de gerenciamento de pedidos para entregadores',
      },
      {
        previewImageSrc: 'assets/images/acougue.png',
        thumbnailImageSrc: 'assets/images/logo_acougue.png',
        title: 'Meu açougue',
        alt: 'Sistema de gestão de açougues',
        transparency: true,
      },
      {
        previewImageSrc: 'assets/images/volcanfly.png',
        thumbnailImageSrc: 'assets/images/logo_volcanfly.png',
        title: 'Volcan Fly',
        alt: 'Escola de parapente em Poços de Caldas - MG',
      }
    );
  }
}
