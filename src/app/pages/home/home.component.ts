import { Component, OnInit } from '@angular/core';
import { WindowUtil } from 'src/app/utils/window.util';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  textsShown: Array<number> = [];
  texts = [
    { label: 'Sua empresa sempre no futuro', mobile: false },
    { label: 'Tecnologias avançadas', mobile: true },
    { label: 'Sites estáticos', mobile: true },
    { label: 'Plataformas web', mobile: true },
    { label: 'Plataformas desktop', mobile: true },
    { label: 'Aplicações móveis', mobile: true },
  ];
  interval: object | undefined;
  position = 0;

  ngOnInit(): void {
    this.startWritting();
  }

  startWritting(): void {
    this.typeWriter();
    this.interval = setInterval(() => {
      this.typeWriter();
    }, 7000);
  }

  typeWriter() {
    this.position = this.randomPosition;
    this.textsShown.push(this.position);
  }

  get randomPosition(): number {
    if (this.textsShown.length === this.texts.length) {
      this.textsShown = [];
    }
    const position = Math.floor(
      Math.random() *
        (this.mobile ? this.mobilesOnly.length : this.texts.length)
    );
    if (this.textsShown.find((x) => x === position)) {
      return this.randomPosition;
    }
    return position;
  }

  get mobilesOnly(): Array<{ label: string; mobile: boolean }> {
    return this.texts.filter((t) => t.mobile);
  }

  get mobile(): boolean {
    return WindowUtil.mobile;
  }
}
