import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-steps-requirements',
  templateUrl: './steps-requirements.component.html',
  styleUrls: ['./steps-requirements.component.scss'],
})
export class StepsRequirementsComponent {
  constructor(private router: Router) {}

  nextPage() {
    this.router.navigate(['steps/personal']);
  }
}
