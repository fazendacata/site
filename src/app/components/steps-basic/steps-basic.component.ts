import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-steps-basic',
  templateUrl: './steps-basic.component.html',
  styleUrls: ['./steps-basic.component.scss'],
})
export class StepsBasicComponent {
  constructor(private router: Router) {}

  nextPage() {
    this.router.navigate(['steps/requirements']);
  }
}
