import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { BroadcastUtil } from 'src/app/utils/broadcast.util';
import { WindowUtil } from 'src/app/utils/window.util';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  title = 'AR Soluções';
  items: MenuItem[] = [];
  display = false;

  constructor(
    private router: Router
  ) {

  }

  ngOnInit() {
    this.items = [
      { label: 'Início', icon: "pi pi-home", url: 'home#home' },
      { label: 'Nossos Projetos', routerLink: 'our-projects', icon: 'pi pi-book' },
      { label: 'Time', url: 'home#team', icon: 'pi pi-users' },
      { label: 'Parceiros', routerLink: 'partners', icon: 'pi pi-id-card' },
      { label: 'Seu Projeto', routerLink: 'steps/basic', icon: 'pi pi-desktop' },
      { label: 'Contato', routerLink: 'contact', icon: 'pi pi-envelope' },
    ];

    BroadcastUtil.get('screenResize').subscribe(() => {
      this.checkScreenSize();
    });

  }

  checkScreenSize(): void {
    if (this.isMobile) {
      this.display = false;
    } else {
      this.display = true;
    }
  }

  goTo(routerLink: string, url?: string) {
    this.router.navigateByUrl((!routerLink && url ? url : routerLink));
  }

  get active(): MenuItem {
    const path = window.location.href;
    return this.items.find((i) => path.includes(i.routerLink as string) || path.includes(i.url as string)) as MenuItem;
  }

  get isMobile(): boolean {
    return WindowUtil.mobile;
  }
}
