import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-steps-personal',
  templateUrl: './steps-personal.component.html',
  styleUrls: ['./steps-personal.component.scss'],
})
export class StepsPersonalComponent {
  constructor(private router: Router) {}

  nextPage() {
    this.router.navigate(['steps/confirmation']);
  }
}
