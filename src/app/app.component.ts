import { Component, HostListener } from '@angular/core';
import { BroadcastUtil } from './utils/broadcast.util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'ARSolucoes';
  innerWidth: number;

  constructor() {
    this.innerWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: { target: { innerWidth: number } }): void {
    this.innerWidth = event.target.innerWidth;
    BroadcastUtil.get('screenResize').emit();
  }
}
