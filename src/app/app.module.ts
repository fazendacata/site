import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { YourProjectComponent } from './pages/your-project/your-project.component';

//PrimeNg
import { TabMenuModule } from 'primeng/tabmenu';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { StepsModule } from 'primeng/steps';
import { StepsBasicComponent } from './components/steps-basic/steps-basic.component';
import { StepsRequirementsComponent } from './components/steps-requirements/steps-requirements.component';
import { StepsPersonalComponent } from './components/steps-personal/steps-personal.component';
import { StepsConfirmationComponent } from './components/steps-confirmation/steps-confirmation.component';
import { ToastModule } from 'primeng/toast';
import { OurProjectsComponent } from './pages/our-projects/our-projects.component';
import { TeamComponent } from './pages/team/team.component';
import { PartnersComponent } from './pages/partners/partners.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SidebarModule } from 'primeng/sidebar';
import { GalleriaModule } from 'primeng/galleria';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    YourProjectComponent,
    StepsBasicComponent,
    StepsRequirementsComponent,
    StepsPersonalComponent,
    StepsConfirmationComponent,
    OurProjectsComponent,
    TeamComponent,
    PartnersComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TabMenuModule,
    StepsModule,
    ToastModule,
    CardModule,
    ButtonModule,
    InputTextareaModule,
    SidebarModule,
    GalleriaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
