export class WindowUtil {
  static get mobile(): boolean {
    return window.innerWidth <= 767;
  }
}
