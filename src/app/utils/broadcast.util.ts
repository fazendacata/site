import { EventEmitter } from "@angular/core";
export class BroadcastUtil {
  private static emitters: {
    [eventName: string]: EventEmitter<object>
  } = {};
  static get(eventName: string): EventEmitter<object> {
    if (!this.emitters[eventName]) {
      this.emitters[eventName] = new EventEmitter<object>();
    }
    return this.emitters[eventName];
  }
}
