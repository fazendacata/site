import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StepsBasicComponent } from './components/steps-basic/steps-basic.component';
import { StepsConfirmationComponent } from './components/steps-confirmation/steps-confirmation.component';
import { StepsPersonalComponent } from './components/steps-personal/steps-personal.component';
import { StepsRequirementsComponent } from './components/steps-requirements/steps-requirements.component';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';
import { OurProjectsComponent } from './pages/our-projects/our-projects.component';
import { PartnersComponent } from './pages/partners/partners.component';
import { TeamComponent } from './pages/team/team.component';

import { YourProjectComponent } from './pages/your-project/your-project.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'your-project', component: YourProjectComponent},
  {path: 'our-projects', component: OurProjectsComponent},
  {path: 'partners', component: PartnersComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'team', component: TeamComponent},
  {path: 'steps/basic', component: StepsBasicComponent},
  {path: 'steps/requirements', component: StepsRequirementsComponent},
  {path: 'steps/personal', component: StepsPersonalComponent},
  {path: 'steps/confirmation', component: StepsConfirmationComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
